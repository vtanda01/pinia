import { createRouter, createWebHashHistory } from "vue-router";
import AboutPage from './pages/AboutPage.vue';
import HomePage from './pages/HomePage.vue';
import Profile from './pages/Profile.vue';
import Product from './pages/product.vue';
import CartPage  from './pages/CartPage.vue';
const router = createRouter({
    history: createWebHashHistory(),
    routes:[
        {
            path: '',
            alias: '/home',
            name: 'home',
            component: HomePage,
            meta: { transition: 'fade' }
        },
        {
        path: '/about',
        name: 'about',
        component: AboutPage,
        meta: { transition: 'fade' }
    },
    {
        
        path: '/product/:id',
        name: 'product',
        component : Product,
        meta: { transition: 'fade' }
    },
    {
        
        path: '/panier',
        name: 'panier',
        component : CartPage,
        meta: { transition: 'fade' }
    },
    {
        path: '/profile',
        name: 'profile',
        component: Profile,
        // meta: { requiresAuth: true } 
        beforeEnter: (to, from, next) => {
            next({
                path: '/home',
                query: { redirect: to.fullPath }
            });
        },
        meta: { transition: 'fade' },

    }
    ],
}) 

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {

        next({
            path: '/home', //ou vers la page de login
            query: { redirect: to.fullPath }
        });
    } else {
        next(); 
    }
});
export default router 